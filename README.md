Instructions pour la configuration du serveur
=============================================

Initialisation du serveur
-------------------------

1.  Compiler les fichiers TypeScript en JavaScript :
    
    sh
    
    ```sh
    npx tsc
    ```
    
2.  Définir le port d'écoute du serveur :
    
    sh
    
    ```sh
    set PING_LISTEN_PORT=8000
    ```
    
3.  Démarrer le serveur :
    
    sh
    
    ```sh
    node build/index.js
    ```
    
4.  Accéder au serveur en naviguant à l'URL suivante dans un navigateur web :
    
    bash
    
    ```bash
    http://localhost:8000/ping
    ```
    
    _Note : Si vous rencontrez une erreur 404, assurez-vous d'avoir ajouté "/ping" à la fin de l'URL._
    

Utilisation de Docker
---------------------

### Configuration initiale

*   Installer et démarrer Docker Desktop.
*   Installer l'extension Docker pour VSCode.

### Construction et exécution du conteneur Docker

1.  Construire l'image Docker :
    
    sh
    
    ```sh
    docker build -t test-node .
    ```
    
2.  Exécuter le conteneur en mappant les ports et en définissant la variable d'environnement :
    
    sh
    
    ```sh
    docker run -it --rm -p 8080:8000 -e PING_LISTEN_PORT=8000 test-node
    ```
    
3.  Tester si la page web fonctionne avec `curl` :
    
    sh
    
    ```sh
    curl http://localhost:8080/ping -v
    ```
    

### Processus de construction à étapes multiples

1.  Commencer la construction à étapes multiples avec :
    
    sh
    
    ```sh
    docker build -t 2e-image -f Dockerfile .
    ```
    
2.  Exécuter le conteneur de manière similaire au processus à étape unique mais avec un mappage de ports différent :
    
    sh
    
    ```sh
    docker run -it --rm -p 8081:8000 -e PING_LISTEN_PORT=8000 test-node
    ```
    
3.  Vérifier que le serveur est actif en utilisant `curl` avec le port modifié :
    
    sh
    
    ```sh
    curl http://localhost:8081/ping -v
    ```
    

Docker Compose
--------------

### Démarrage du serveur

1.  Pour lancer le serveur avec Docker Compose, exécutez :
    
    sh
    
    ```sh
    docker compose up
    ```
    
2.  Si le serveur ne démarre pas, forcez une reconstruction avec :
    
    sh
    
    ```sh
    docker compose up --build
    ```
    

### Vérification

Après le démarrage du serveur, vous devriez avoir 4 API actives sur le port 8000. Pour tester, exécutez une commande `curl` sur le port 8081 depuis un autre terminal et observez les connexions sur le terminal initial :

sh

```sh
curl http://localhost:8081/ping
```

Veuillez suivre ces instructions attentivement pour assurer une configuration et un déploiement sans problème de votre environnement serveur.
